const express = require("express")
const app = express();
const http = require("http");
const Socket = require("socket.io");
const session = require('express-session');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');

const mysql = require('mysql');
require("dotenv").config();

const host = process.env.host;
const database = process.env.database;
const username = process.env.user;
const password = process.env.password;


const server = http.createServer(app);
const io = Socket(server);

const connection = mysql.createConnection({
    host: host,
    user: username,
    password: password,
    database: database 
});

connection.connect((err) =>{
    if(err){
        console.log("Error Connecting to database" + err.stack)
        return
    }
    console.log("Connected to db successfuly")
})
app.use(bodyParser.urlencoded({extended : true}))

app.use(express.static('public'));

app.use(session({
    secret: 'secret', // Change this to a long random string in production
    resave: true,
    saveUninitialized: true
  }));



//geting directory files 
app.get('/', (req, res)=>{
    res.sendFile(__dirname + '/login.html')
});

app.get('/management', (req, res)=>{

    if (!req.session.userId) {
        res.redirect('/');
        return;
      }
    res.sendFile(__dirname + '/index.html')
});
app.get('/register', (req, res) => {
    res.sendFile(__dirname + '/register.html');
  });

app.post('/register', (req, res) => {
    try{
      const { name, username, password } = req.body;
        
      connection.query('SELECT * FROM user WHERE username = ?', [username], async (err, results, fields) => {
        if (err) {
          console.error('Error finding data: ' + err.stack);
          return;
        }
        console.log(results)
        if (results.length !== 0) {
            return res.status(400).send('Username already exists');
          }
          console.log(password)
          const hashedPassword = await bcrypt.hash(password, 10);
          const currentDate = new Date();
          const empData = {username : username, password : hashedPassword, name : name, created_at : currentDate}
          connection.query('INSERT INTO user SET ?', empData, (err, result) => {
            if (err) {
              console.error('Error inserting data: ' + err.stack);
              return;
            }
            console.log('Inserted ID:', result.insertId);
            res.redirect('/'); 
         }); 
      
          
     }); 
    } catch (error) {
          
        res.status(500).send('Internal Server Error');
      }
    })

    app.post('/login', async (req, res) => {
        try {
          const { username, password } = req.body;
      
          
          connection.query('SELECT * FROM user WHERE username = ?', [username], async (err, results, fields) => {
            if (err) {
                console.error('Error finding data: ' + err.stack);
                return;
              }
              
              if (results.length !== 0 && await bcrypt.compare(password, results[0].password) ) {

                req.session.userId = results[0].id;
                res.redirect('/management'); 
              } else {
                
                res.status(401).send('Invalid username or password');
              }

          })
          
        } catch (error) {
          
          res.status(500).send('Internal Server Error');
        }
      });
      
      
  
      
      
    
//initializing connection with socket io
io.on("connection", (socket)=>{
    console.log("New Client detected", socket.id);

    function getEmployeeData(){
        connection.query('SELECT * FROM employee', (err, rows) => {
            if (err) {
              console.error('Error executing query: ' + err.stack);
              return;
            }
            //console.log('Query result:', rows);
            io.emit('getEmp', rows)
          });
    }
    getEmployeeData();
    
    socket.on('addEmployee', (data)=>{
        const currentDate = new Date();

        const empData = {first_name : data.fname, last_name : data.lname, position : data.position, created_at : currentDate}
        connection.query('INSERT INTO employee SET ?', empData, (err, result) => {
            if (err) {
              console.error('Error inserting data: ' + err.stack);
              return;
            }
            console.log('Inserted ID:', result.insertId);
            getEmployeeData();
         }); 
       })


    socket.on('updateEmployee', (data, id)=>{
        console.log("update")
        const currentDate = new Date();
        const empData = {first_name : data.fname, last_name : data.lname, position : data.position, updated_at : currentDate}

        
        connection.query('UPDATE employee SET ? WHERE id = ?', [empData, id], (err, result) => {
        if (err) {
            console.error('Error updating data: ' + err.stack);
            return;
        }
        console.log('Updated rows:', result.affectedRows);
        getEmployeeData();
        });
     })   

     socket.on('deleteEmployee', (id)=>{

        connection.query('DELETE FROM employee WHERE id = ?', [id], (err, result) => {
            if (err) {
              console.error('Error deleting data: ' + err.stack);
              return;
            }
            console.log('Deleted rows:', result.affectedRows);
            getEmployeeData();
          });
     })
})

server.listen(3000, ()=>{
    console.log("Server is running........")
})